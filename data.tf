data "aws_ecs_cluster" "selected" {
  cluster_name = var.cluster_name
}

data "aws_lb" "selected" {
  name = var.load_balancer_name
}

data "aws_lb_listener" "selected80" {
  count             = var.is_https ? 0 : 1
  load_balancer_arn = data.aws_lb.selected.arn
  port              = 80
}

data "aws_lb_listener" "selected443" {
  count             = var.is_https ? 1 : 0
  load_balancer_arn = data.aws_lb.selected.arn
  port              = 443
}

data "aws_region" "current" {}

data "aws_security_group" "selected" {
  id = one(data.aws_lb.selected.security_groups)
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.selected.id
  dynamic "filter" {
    for_each = var.filter_subnets_by_tags
    content {
      name   = "tag:${filter.value["key"]}"
      values = var.filter.value["values"]
    }
  }
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}
