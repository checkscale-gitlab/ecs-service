[
    {
      "name": "${container_name}",
      "image": "${image}:${image_tag}",
      "portMappings": [
        {
          "containerPort": ${container_port}
        }
      ],
      "networkMode": "awsvpc",
      "essential": true,
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${log_group}",
          "awslogs-region": "${region}",
          "awslogs-stream-prefix": "${container_name}"
        }
      },
      "secrets": ${secrets},
      "environment": ${environment}
    }
]