resource "aws_ecs_service" "this" {
  name                    = var.app_name
  task_definition         = aws_ecs_task_definition.this.arn
  cluster                 = data.aws_ecs_cluster.selected.id
  launch_type             = var.launch_type
  desired_count           = var.min_tasks
  enable_ecs_managed_tags = true
  propagate_tags          = "SERVICE"

  network_configuration {
    security_groups  = [aws_security_group.this.id]
    subnets          = data.aws_subnet_ids.subnets.ids
    assign_public_ip = false
  }

  dynamic "load_balancer" {
    for_each = [var.load_balancer_name != "" ? 1 : 0]
    content {
      target_group_arn = aws_lb_target_group.this[0].arn
      container_name   = var.app_name
      container_port   = var.app_port
    }
  }

  lifecycle {
    ignore_changes = [desired_count]
  }
}
