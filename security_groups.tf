resource "aws_security_group" "this" {
  name        = "${var.app_name}-sg"
  description = "${var.app_name} security group"
  vpc_id      = data.aws_vpc.selected.id

  egress {
    cidr_blocks = [data.aws_vpc.selected.cidr_block]
    description = "vpc access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:AWS009
    description = "Allow access to aws apis and any other https resource"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = { Name = "${var.app_name}-sg" }
}

# Allow access from load balancer
resource "aws_security_group_rule" "load_balancer" {
  count                    = var.load_balancer_name != "" ? 1 : 0
  description              = "load-balancer-to-me"
  from_port                = var.app_port
  to_port                  = var.app_port
  protocol                 = "tcp"
  type                     = "ingress"
  security_group_id        = aws_security_group.this.id
  source_security_group_id = data.aws_security_group.selected.id
}
